module gitlab.com/ariews/cache

go 1.13

require (
	github.com/go-redis/cache/v8 v8.1.1
	github.com/go-redis/redis/v8 v8.3.2
	github.com/vmihailenco/go-tinylfu v0.1.0
)
