package cache

import (
	"context"
	"sync"
	"time"

	"github.com/go-redis/cache/v8"
	"github.com/go-redis/redis/v8"
	"github.com/vmihailenco/go-tinylfu"
)

var locker sync.Map

// Cache hehe
type Cache struct {
	ttl   time.Duration
	ctx   context.Context
	cache *cache.Cache
}

// Read cache key
func (c *Cache) Read(key string) (out interface{}, err error) {
	err = c.cache.Get(c.ctx, key, &out)

	return
}

// Delete delete cache by key
func (c *Cache) Delete(key string) error {
	return c.cache.Delete(c.ctx, key)
}

// Write set cache
func (c *Cache) Write(key string, value interface{}) error {
	item := &cache.Item{
		Ctx:   c.ctx,
		Key:   key,
		Value: value,
		TTL:   c.ttl,
	}

	return c.cache.Set(item)
}

// Fetch fetch cache
func (c *Cache) Fetch(key string, compute func() (interface{}, error)) (interface{}, error) {
	if _, loaded := locker.LoadOrStore(key, true); loaded {
		for {
			if _, loaded := locker.Load(key); !loaded {
				value, err := c.Read(key)
				return value, err
			}
			time.Sleep(10 * time.Millisecond)
		}
	}
	defer locker.Delete(key)

	value, err := c.Read(key)
	if err != nil {
		value, err = compute()
		if err != nil {
			return value, err
		}

		err = c.Write(key, value)
	}

	return value, err
}

func createRedisClient(hosts map[string]string) *cache.Cache {
	ring := redis.NewRing(&redis.RingOptions{Addrs: hosts})
	return cache.New(&cache.Options{
		Redis:      ring,
		LocalCache: tinylfu.NewSync(10000, 100000),
	})
}

// redis cache
var rdc *Cache

// MakeCache create/return cache instance
func MakeCache(hosts map[string]string, ttl time.Duration) *Cache {
	if rdc == nil {
		rdc = NewCache(hosts, ttl)
	}
	return rdc
}

// NewCache new Cache instance
func NewCache(hosts map[string]string, ttl time.Duration) *Cache {
	return &Cache{
		cache: createRedisClient(hosts),
		ctx:   context.TODO(),
		ttl:   ttl,
	}
}
